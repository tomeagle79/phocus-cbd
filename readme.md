# Useful stuff for shopify themeing

1. Create a new store on shopify
2. Download shopify themekit, add to PATH and install CLI https://shopify.github.io/themekit/
3. Good guide for shopify workflow: https://www.youtube.com/watch?v=1xWFsYmBoX0, https://www.youtube.com/watch?v=78N7hRwIZO4
3. Create a new private app at /admin/apps/private
4. Copy the password to settings.yml
5. Set 'Theme templates and theme assets' to read and write

## Useful themekit commands
1. theme download - download the live theme
2. theme watch - watch the directory for changes

## Other useful stuff
1. To create a stylesheet, eg: about.scss.liquid and then:
2. Add this: {{ "about.scss" | asset_url | stylesheet_tag }} to theme.liquid to include
3. Change google maps iframe: https://www.templatemonster.com/help/shopify-how-to-change-your-store-address-and-google-map-location.html