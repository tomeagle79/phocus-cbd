$(document).ready(function () {
    // General carousels
    $slick_defaults = {
        autoplay: false,
        slidesToShow: 1,
        arrows: true,
        dots: true,
        centerMode: false,
        centerPadding: '',
    }
    $('.carousel').each(function () {
        var $carousel = $(this);
        if ($carousel.data('slick-options') !== "") {
            if (typeof $carousel.data('slick-options') == "string") {
                var $slick_options = JSON.parse($carousel.data('slick-options'));
            } else {
                var $slick_options = $carousel.data('slick-options')
            }

            $carousel.slick($slick_options)
        } else {
            $carousel.slick($slick_defaults);
        }
    });

    // Mobile carousels
    $(window).on('load resize orientationchange', function () {
        var $carousel = $('.carousel-mobile');
        // Set initial slide to show
        var slide = 1;
        $carousel.each(function () {
            if ($(window).outerWidth() >= 768) {
                if ($carousel.hasClass('slick-initialized')) {
                    $carousel.slick('unslick');
                }
            } else {
                if (!$carousel.hasClass('slick-initialized')) {
                    $carousel.slick({
                        infinite: false,
                        arrows: false,
                        dots: true,
                        slidesToShow: 1,
                        centerMode: true,
                        centerPadding: '35px',
                        mobileFirst: true,
                        initialSlide: slide,
                        infinite: true
                    });
                }
            } // else
        });
    }); // end window load
});